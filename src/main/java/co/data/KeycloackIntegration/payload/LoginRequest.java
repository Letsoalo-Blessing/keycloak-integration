package co.data.KeycloackIntegration.payload;

import lombok.Getter;

@Getter
public class LoginRequest {
    String username;
    String password;
}
