package co.data.KeycloackIntegration.utils;

public class ApiDocConstantUtil {

    public static final String TITLE = "Keycloak-Integration Service integration REST API";
    public static final String DESCRIPTION = "This technical specification provides information about how to use the Keycloak-Integration Service API.";
    public static final String VERSION = "1.0";
    public static final String TERMS_OF_SERVICE = "Terms of Services";
    public static final String LICENSE = "Apache 2.0";
    public static final String LICENSE_URL = "http://wwww.apache.org/licenses/LICENSE-2.0";
    public static final String URL = "";
    public static final String EMAIL = "Dev : Blessing <letsoaloblessing2@gmail.com>";
}
